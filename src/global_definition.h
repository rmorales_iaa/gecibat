/*
 * global_var.h
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef GLOBAL_DEFINITION_H_
#define GLOBAL_DEFINITION_H_
//=============================================================================
#include "type_definition.h"
  //---------------------------------------------------------------------------
  #define CSV_LINE_SIZE   1024
  //---------------------------------------------------------------------------
  #define ITEM_DIVIDER    ","
  //---------------------------------------------------------------------------
//=============================================================================
#endif /* GLOBAL_DEFINITION_H_ */
