/*
 * fits.c
 *
 *  Created on: 30 Oct 2021
 *      Author: rafa
 */
//============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//============================================================================
#include "fits.h"
#include "../logger/logger.h"
#include "../util/my_util.h"
#include "global_definition.h"
//============================================================================
//----------------------------------------------------------------------------
FILE * fits_file;
uint64_t processed_record_count = 0;
//----------------------------------------------------------------------------
#define FITS_RECORD_TEMPLATE_HEADER_COUNT 5
char * FITS_RECORD_TEMPLATE_HEADER[FITS_RECORD_TEMPLATE_HEADER_COUNT] = {
    "SIMPLE  =                    T / conforms to FITS standard                      "
  , "BITPIX  =                    8 / array data type                                "
  , "NAXIS   =                    0 / number of array dimensions                     "
  , "EXTEND  =                    T                                                  "
  , "END                                                                             "
};
//----------------------------------------------------------------------------
char * FITS_RECORD_EMPTY = "                                                                                ";
//----------------------------------------------------------------------------
#define FITS_RECORD_TABLE_HEADER_COUNT 17
char * FITS_RECORD_TABLE_HEADER[FITS_RECORD_TABLE_HEADER_COUNT] = {
    "XTENSION= 'BINTABLE'           / binary table extension                         "    //0
  , "BITPIX  =                    8 / array data type                                "    //1
  , "NAXIS   =                    2 / number of array dimensions                     "    //2
  , "NAXIS1  =                   32 / byte size of each record (all columns)         "    //3
  , "NAXIS2  =           1811709771 / number of records                              "    //4 //it will be updated before saving
  , "PCOUNT  =                    0 / number of group parameters                     "    //5
  , "GCOUNT  =                    1 / number of groups                               "    //6
  , "TFIELDS =                    4 / number of columns                              "    //7
  , "TTYPE1  = 'ID      '                                                            "    //8
  , "TFORM1  = 'K       '                                                            "    //9
  , "TTYPE2  = 'RA      '                                                            "    //10
  , "TFORM2  = 'D       '                                                            "    //11
  , "TTYPE3  = 'DEC     '                                                            "    //12
  , "TFORM3  = 'D       '                                                            "    //13
  , "TTYPE4  = 'FLUX    '                                                            "    //14
  , "TFORM4  = 'D       '                                                            "    //15
  , "END                                                                             "    //16
};

//----------------------------------------------------------------------------
#define FITS_RECORD_TABLE_HEADER_COUNT_SIMPLE_CONVERSION 17
char * FITS_RECORD_TABLE_HEADER_SIMPLE_CONVERSION[FITS_RECORD_TABLE_HEADER_COUNT_SIMPLE_CONVERSION] = {
    "XTENSION= 'BINTABLE'           / binary table extension                         "    //0
  , "BITPIX  =                    8 / array data type                                "    //1
  , "NAXIS   =                    2 / number of array dimensions                     "    //2
  , "NAXIS1  =                   32 / byte size of each record (all columns)         "    //3
  , "NAXIS2  =                    0 / number of records                              "    //4   //it will be updated before saving
  , "PCOUNT  =                    0 / number of group parameters                     "    //5
  , "GCOUNT  =                    1 / number of groups                               "    //6
  , "TFIELDS =                    4 / number of columns                              "    //7
  , "TTYPE1  = 'FIELD_X'                                                             "    //8
  , "TFORM1  = 'D17.12 '                                                             "    //9
  , "TTYPE2  = 'FIELD_Y'                                                             "    //10
  , "TFORM2  = 'D17.12 '                                                             "    //11
  , "TTYPE3  = 'INDEX_RA'                                                            "    //12
  , "TFORM3  = 'D17.12 '                                                             "    //13
  , "TTYPE4  = 'INDEX_DEC'                                                           "    //14
  , "TFORM4  = 'D17.12 '                                                             "    //15
  , "END                                                                             "    //16
};
//----------------------------------------------------------------------------
uint8_t store(uint8_t * destination, uint8_t * source, uint8_t n) {
  for(uint8_t i=0;i<n;++i)
    destination[n-i-1] = source[i];
  return n;
}
//----------------------------------------------------------------------------
bool process_record(char * line, uint64_t line_count) {
  trim(line);
  if (strlen(line) == 0) return true;

  char * endChar;
  uint8_t row[FITS_TABLE_RECORD_SIZE];

  uint64_t id  = atol(strtok(line, ITEM_DIVIDER));
  double ra    = strtod(strtok(NULL, ITEM_DIVIDER),& endChar);
  double dec   = strtod(strtok(NULL, ITEM_DIVIDER),& endChar);
  double flux  = strtod(strtok(NULL, ITEM_DIVIDER),& endChar);

  uint8_t offset = store(row,(uint8_t *)&id,sizeof(id));
  offset += store(row + offset, (uint8_t *) &ra, sizeof(ra));
  offset += store(row + offset, (uint8_t *) &dec, sizeof(dec));
  offset += store(row + offset, (uint8_t *) &flux, sizeof(flux));

  ++processed_record_count;

  if (!fits_add_partial_record(row, offset)) return false;

  return true;
}

//----------------------------------------------------------------------------
bool process_record_simple_convesion(char * line, uint64_t line_count) {
  trim(line);
  if (strlen(line) == 0) return true;

  char * endChar;
  uint8_t row[FITS_TABLE_RECORD_SIZE];

  double x   = strtod(strtok(line, ","),&endChar);
  double y   = strtod(strtok(NULL, ","),&endChar);
  double ra  = strtod(strtok(NULL, ","),&endChar);
  double dec = strtod(strtok(NULL, ","),&endChar);

  uint8_t offset = store(row,(uint8_t *)&x,sizeof(x));
  offset += store(row + offset, (uint8_t *) &y, sizeof(y));
  offset += store(row + offset, (uint8_t *) &ra, sizeof(ra));
  offset += store(row + offset, (uint8_t *) &dec, sizeof(dec));

  ++processed_record_count;

  if (!fits_add_partial_record(row, offset)) return false;

  return true;
}
//----------------------------------------------------------------------------
bool fits_create(char * file_name, char * mode) {
  uint64_t file_byte_size = file_open(file_name, &fits_file, mode);
  if (!strcmp(mode,"a")) return file_byte_size != 0;
  else return true;
}
//----------------------------------------------------------------------------
bool fits_add_record(uint8_t * record) {
  size_t bytes_written = fwrite(record, 1, RECORD_BYTE_SIZE, fits_file);
  if (bytes_written != RECORD_BYTE_SIZE) {
	add_string_to_logger("Error writing a record in the FITS file");
	return false;
  }
  else return true;
}
//----------------------------------------------------------------------------
bool fits_add_partial_record(uint8_t * record, uint64_t byte_size) {
  size_t bytes_written = fwrite(record, 1, byte_size, fits_file);
  if (bytes_written != byte_size) {
	add_string_to_logger("Error writing a record in the FITS file");
	return false;
  }
  else return true;
}
//----------------------------------------------------------------------------
bool fits_close(char * file_name) {
  return file_close(file_name, fits_file);
}
//----------------------------------------------------------------------------
bool fits_write_min_header(void) {
  for(int i=0; i<FITS_RECORD_TEMPLATE_HEADER_COUNT;++i)
	  if (!fits_add_record((uint8_t *)FITS_RECORD_TEMPLATE_HEADER[i])) return false;

  for(int i=FITS_RECORD_TEMPLATE_HEADER_COUNT; i<RECORDS_PER_BLOCK;++i)
	  if (!fits_add_record((uint8_t *)FITS_RECORD_EMPTY)) return false;

  return true;
}

//----------------------------------------------------------------------------
bool fits_write_table_header(char * record) {

  for(int i=0; i<4;++i)
    if (!fits_add_record((uint8_t *)FITS_RECORD_TABLE_HEADER[i])) return false;

  if (!fits_add_record((uint8_t *)record)) return false;

  for(int i=5; i<FITS_RECORD_TABLE_HEADER_COUNT;++i)
    if (!fits_add_record((uint8_t *)FITS_RECORD_TABLE_HEADER[i])) return false;

  for(int i=FITS_RECORD_TABLE_HEADER_COUNT; i<RECORDS_PER_BLOCK;++i)
    if (!fits_add_record((uint8_t *)FITS_RECORD_EMPTY)) return false;

  return true;
}

//----------------------------------------------------------------------------
bool fits_write_table_header_simple_conversion(char * record) {

  for(int i=0; i<4;++i)
    if (!fits_add_record((uint8_t *)FITS_RECORD_TABLE_HEADER_SIMPLE_CONVERSION[i])) return false;

  if (!fits_add_record((uint8_t *)record)) return false;

  for(int i=5; i<FITS_RECORD_TABLE_HEADER_COUNT_SIMPLE_CONVERSION;++i)
      if (!fits_add_record((uint8_t *)FITS_RECORD_TABLE_HEADER_SIMPLE_CONVERSION[i])) return false;

  for(int i=FITS_RECORD_TABLE_HEADER_COUNT_SIMPLE_CONVERSION; i<RECORDS_PER_BLOCK;++i)
    if (!fits_add_record((uint8_t *)FITS_RECORD_EMPTY)) return false;

  return true;
}
//----------------------------------------------------------------------------
bool finish_binary_table(uint64_t padding_byte_size) {
  uint8_t * padding = (uint8_t *) malloc(padding_byte_size);
  memset(padding,0,padding_byte_size);

  if (!fits_add_partial_record(padding,padding_byte_size)){
	free(padding);
	return false;
  }
  free(padding);
  return true;
}
//============================================================================
