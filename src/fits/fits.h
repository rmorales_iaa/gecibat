/*
 * fits.h
 *
 *  Created on: 30 Oct 2021
 *      Author: rafa
 */
//============================================================================
#ifndef FITS_FITS_H_
#define FITS_FITS_H_
  //--------------------------------------------------------------------------
  #include <stdbool.h>
  #include <stdint.h>
  //--------------------------------------------------------------------------
  #define RECORD_BYTE_SIZE       80
  #define RECORDS_PER_BLOCK      36
  #define BLOCK_BYTE_SIZE (RECORD_BYTE_SIZE * RECORDS_PER_BLOCK)
  //--------------------------------------------------------------------------
  //same value as record 'NAXIS1' in array 'FITS_RECORD_TABLE_HEADER'
  #define FITS_TABLE_RECORD_SIZE  32

  bool process_record(char * line, uint64_t line_count);
  bool process_record_simple_convesion(char * line, uint64_t line_count);
  //--------------------------------------------------------------------------
  bool fits_create(char * file_name, char * mode);
  bool fits_add_record(uint8_t * record);
  bool fits_close(char * file_name);

  bool fits_write_min_header(void);

  bool fits_write_table_header(char * record);
  bool fits_write_table_header_simple_conversion(char * record);

  bool finish_binary_table(uint64_t padding_byte_size);

  bool fits_add_partial_record(uint8_t * record, uint64_t byte_size);
  //--------------------------------------------------------------------------
#endif /* FITS_FITS_H_ */
//============================================================================
