/*
 * logger.c
 *
 *  Created on: 17 ago. 2018
 *      Author: rafa
 */
//---------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <stdarg.h>
//---------------------------------------------------------------------------
#include "../util/my_util.h"
//---------------------------------------------------------------------------
void print_log_string( char * message ) {

  char log_buffer[2048];
  struct timeval curTime;
  gettimeofday(&curTime, NULL);
  int milli = curTime.tv_usec / 1000;

  log_buffer[0]=0;
  time_t now = time(0);
  strftime(log_buffer, 100, "%Y-%m-%d %H:%M:%S", localtime (&now));

  char s_i[100];
  s_i[0]=0;
  snprintf(s_i, 100, "%03d",milli);

  strcat (log_buffer, ".");
  strcat (log_buffer, s_i);
  strcat (log_buffer, " :: ");
  strcat (log_buffer, message);

  printf ("%s\n", log_buffer);
}

//---------------------------------------------------------------------------
void add_to_logger(char * start_message, char *format, ...) {
  char local_value[100];
  char log_buffer[1024];
  log_buffer[0] = 0;

  va_list ap;

  uint8_t c;
  uint16_t h;
  uint32_t l;
  largeInt L;

  float f;
  double F;
  char *s;

  va_start(ap, format);

  strcat(log_buffer, start_message);

  for(; *format != '\0'; format++) {
    local_value[0] = 0;

 	switch(*format) {
      case 'h': // 16-bit
        h = (uint16_t)va_arg(ap, int); // promoted
		snprintf(local_value, 100, "%d", h);
		strcat(log_buffer, local_value);
		break;

      case 'l': // 32-bit
		l = va_arg(ap, uint32_t);
		snprintf(local_value, 100, "%i", l);
		strcat(log_buffer, local_value);
		break;

      case 'L': // 64-bit
	    L = va_arg(ap, largeInt);
		snprintf(local_value, 100, "%lld", L);
		strcat(log_buffer, local_value);
		break;

  	  case 'c': // 8-bit
		c = (uint8_t)va_arg(ap, int); // promoted
		snprintf(local_value, 100, "%d", c);
		strcat(log_buffer, local_value);
		break;

  	  case 'f': // float
		f = (float)va_arg(ap, double); // promoted
		snprintf(local_value, 100, "%.3f", f);
		strcat(log_buffer, local_value);
		break;

	  case 'F': // float-64
		F = (double)va_arg(ap, double);
		snprintf(local_value, 100, "%.3f", F);
		strcat(log_buffer, local_value);
		break;

      case 's': // string
		s = va_arg(ap, char*);
		strcat(log_buffer, s);
		break;

  	  default :
		printf("\nUnknown formatter %s", format);
      }
  }
  print_log_string(log_buffer);
  va_end(ap);
}

//---------------------------------------------------------------------------
void add_string_to_logger(char * message) {
  add_to_logger(message,"");
}
//---------------------------------------------------------------------------
void add_string_seq_to_logger(int message_count, ...) {
  va_list ap;
  int i;
  char final_message[2048];
  *final_message = 0;

  va_start(ap, message_count);
  for(i = 0; i < message_count; i++)
     strcat(final_message, va_arg(ap, char *));

  va_end(ap);

  add_to_logger(final_message,"");
}
//---------------------------------------------------------------------------
