/*
 * GECIBAT_c.h
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */
//============================================================================
#ifndef GECIBAT_VERSION_H_
#define GECIBAT_VERSION_H_
//============================================================================
#define GECIBAT_C_VERSION_DATE         "Thu 16 Jun 18:52:00 CEST 2022"
#define GECIBAT_C_VERSION_MAYOR        "0"
#define GECIBAT_C_VERSION_MINOR        "2"
#define GECIBAT_C_VERSION_COMPILATION  "2"
//============================================================================
#endif /* GECIBAT_VERSION_H_ */
//============================================================================
