/*
 * util.c
 *
 *  Created on: 8 Nov 2018
 *      Author: rafa
 */
//---------------------------------------------------------------------------
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
//---------------------------------------------------------------------------
#include "my_util.h"
#include "../logger/logger.h"
//---------------------------------------------------------------------------
uint64_t get_ms(void) {
	struct timeval te;
    gettimeofday(&te, NULL); // get current time
    uint64_t milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    return milliseconds;
}
//---------------------------------------------------------------------------
void get_string_date_from_ms(uint64_t ms, int buffer_size, char * buffer) {

	time_t seconds = (time_t)(ms/1000);
	struct tm * t = localtime(&seconds);
	strftime (buffer, buffer_size, "%Y-%m-%d %H:%M:%S", t);
}
//---------------------------------------------------------------------------
void sleep_ms(int milliseconds)
{
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
}

//---------------------------------------------------------------------------
void char_array_to_hex_string(int size, unsigned char * array, char * output){
	char *ptr = &output[0];
	for (int i = 0; i < size; i++)
		ptr += sprintf (ptr, "%02X ", array[i]);
}

//---------------------------------------------------------------------------
void long_to_hex_string(largeInt v, char * output){
	 sprintf(output,"%02LX",v);
}

//---------------------------------------------------------------------------
void store_uint8(uint8_t *buf, uint8_t i){

	*buf++ = i;
}
//---------------------------------------------------------------------------
void store_uint16(uint8_t *buf, uint16_t i)
{
	uint16_t i2 = i;

	*buf++ = i2>>8; *buf++ = i2;
}

//---------------------------------------------------------------------------
void store_uint32(uint8_t *buf, uint32_t i)
{
	uint32_t i2 = i;

	*buf++ = i2>>24; *buf++ = i2>>16;
	*buf++ = i2>>8;  *buf++ = i2;
}

//---------------------------------------------------------------------------
void store_uint64(uint8_t *buf, uint64_t i)
{
	uint64_t i2 = i;

	*buf++ = i2>>56; *buf++ = i2>>48;
	*buf++ = i2>>40; *buf++ = i2>>32;
	*buf++ = i2>>24; *buf++ = i2>>16;
	*buf++ = i2>>8;  *buf++ = i2;
}

//---------------------------------------------------------------------------
uint8_t store_value(uint8_t * buf, uint64_t v, uint8_t type_byte_size)
{
	switch(type_byte_size){
		case 1: { store_uint8(buf,  (uint8_t)v);  return type_byte_size;}
		case 2: { store_uint16(buf, (uint16_t)v); return type_byte_size;}
		case 4: { store_uint32(buf, (uint32_t)v); return type_byte_size;}
		case 8: { store_uint64(buf, (uint64_t)v); return type_byte_size;}
		default : add_string_to_logger("Store_value. Unknown type byte size");
	}
	return 0;
}
//---------------------------------------------------------------------------
uint8_t load_uint8(uint8_t *buf)
{
	return buf[0];
}
//---------------------------------------------------------------------------
uint16_t load_uint16(uint8_t *buf)
{
	uint16_t i2 = ((uint16_t)buf[0]<<8) | buf[1];
	int16_t i;

	// change unsigned numbers to signed
	if (i2 <= 0x7fffu) { i = i2; }
	//else { i = -(int16_t)((uint16_t)0xffff - i2 + (uint16_t)1u); }
	else { i = -1 - (uint16_t)(0xffffu - i2); }

	return i;
}
//---------------------------------------------------------------------------
uint32_t load_uint32(uint8_t *buf)
{
	uint32_t i2 = ((uint32_t)buf[0]<<24) | ((uint32_t)buf[1]<<16) |
	              ((uint32_t)buf[2]<<8)  | buf[3];
	int32_t i;

	// change unsigned numbers to signed
	if (i2 <= 0x7fffffffu) { i = i2; }
	else { i = -1 - (int32_t)(0xffffffffu - i2); }

	return i;
}

//---------------------------------------------------------------------------
uint64_t load_uint64(uint8_t *buf)
{
	uint64_t i2 = ((uint64_t)buf[0]<<56) | ((uint64_t)buf[1]<<48) |
	              ((uint64_t)buf[2]<<40) | ((uint64_t)buf[3]<<32) |
	              ((uint64_t)buf[4]<<24) | ((uint64_t)buf[5]<<16) |
	              ((uint64_t)buf[6]<<8)  | buf[7];
	int64_t i;

	// change unsigned numbers to signed
	if (i2 <= 0x7fffffffffffffffu) { i = i2; }
	else { i = -1 -(int64_t)(0xffffffffffffffffu - i2); }

	return i;
}

//---------------------------------------------------------------------------
uint64_t load_value(uint8_t * buf,  uint8_t type_byte_size)
{
	switch(type_byte_size){
		case 1: return load_uint8(buf);
		case 2: return load_uint16(buf);
		case 4: return load_uint32(buf);
		case 8: return load_uint64(buf);
		default : add_string_to_logger("Load_value. Unknown type byte size");
	}
	return -1;
}
//---------------------------------------------------------------------------
void trim(char * string) {
  int i;
  int stPos,endPos;
  int len=strlen(string);
  for(stPos=0;stPos<len && string[stPos]==' ';++stPos);
  for(endPos=len-1;endPos>=0 && (string[endPos]==' ' || string[endPos]=='\n');--endPos);
  for (i=0; i<=(endPos-stPos); i++)
    string[i] = string[i+stPos];
  string[i] = '\0'; // terminate the string and discard the remaining spaces.
}
//---------------------------------------------------------------------------
int strpos(char *hay, char *needle, int offset)
{
   char haystack[strlen(hay)];
   strncpy(haystack, hay+offset, strlen(hay)-offset);
   char *p = strstr(haystack, needle);
   if (p)
      return p - haystack+offset;
   return -1;
}
//---------------------------------------------------------------------------
uint64_t file_open(const char * file_name, FILE ** file, char * mode) {
  *file = fopen(file_name, mode);
  struct stat st;
  char s[128];

  if(*file == NULL) {
    add_string_seq_to_logger(3, "Can not open file: '", file_name,"'");
    return 0;
  }

  stat(file_name, &st);
  uint64_t csv_file_byte_size =  st.st_size;
  snprintf(s, sizeof(s), "%ld", csv_file_byte_size);
  add_string_seq_to_logger(5, "File: '", file_name,"' opened. File size: ", s, "(bytes)");
  return csv_file_byte_size;
}
//---------------------------------------------------------------------------
bool file_close(const char * file_name, FILE * file) {
  bool r = fclose(file);
  if(r != 0) {
	 add_string_seq_to_logger(3, "Error closing file: '", file_name,"'");
     return false;
   }
  add_string_seq_to_logger(3, "File: '", file_name,"' closed");
  return true;
}

//---------------------------------------------------------------------------
void system_call(const char * command, char * result) {
  const int max_buffer = 1024;
  char buffer[max_buffer];
  result[0]= '\0';
  buffer[0]= '\0';
  FILE * stream = popen(command, "r");

  if (stream) {
     while (!feof(stream))
       if (fgets(buffer, max_buffer, stream) != NULL) strcat(result,buffer);
     pclose(stream);
   }
  trim(result);
}
//---------------------------------------------------------------------------
unsigned long get_line_count(char * file_name) {

  char command[1024];
  char result[1024];

  command[0]='\0';
  result[0]='\0';
  strcpy(command,"wc -l '");
  strcat(command,file_name);
  strcat(command,"' | cut -d' ' -f 1");

  system_call(command, result);
  if (strlen(result) == 0){
    add_string_seq_to_logger(3, "Error getting the line count of file: '", file_name,"'");
    return 0L;
  }

  char *eptr;
  unsigned long r = strtoul(result,&eptr,10);

  if (r == 0) {
    /* If a conversion error occurred, display a message and exit */
    if (errno == EINVAL){
       printf("Conversion error occurred: %d\n", errno);
       return 0;
    }

    /* If the value provided was out of range, display a warning message */
    if (errno == ERANGE) {
       printf("The value provided was out of range\n");
       return 0;
    }
  }
  return r;
}
//---------------------------------------------------------------------------
