/*
 * util.h
 *
 *  Created on: 8 Nov 2018
 *      Author: rafa
 */
//---------------------------------------------------------------------------
#ifndef UTIL_MY_UTIL_H_
#define UTIL_MY_UTIL_H_
  //-----------------------------------------------------------------------
  #include "type_definition.h"
  //-----------------------------------------------------------------------
  #include <sys/types.h>
  #include <stdint.h>
  #include <stdbool.h>
  #include <stdio.h>
  //-----------------------------------------------------------------------
  uint64_t get_ms(void);
  void get_string_date_from_ms(uint64_t ms, int buffer_size, char * buffer);
  //-----------------------------------------------------------------------
  void sleep_ms(int milliseconds);
  void char_array_to_hex_string(int size, unsigned char * array, char * output);
  void long_to_hex_string(largeInt v, char * output);
  //-----------------------------------------------------------------------
  void store_uint8(uint8_t *buf, uint8_t i);
  void store_uint16(uint8_t *buf, uint16_t i);
  void store_uint32(uint8_t *buf, uint32_t i);
  void store_uint64(uint8_t *buf, uint64_t i);
  uint8_t store_value(uint8_t * buf, uint64_t v, uint8_t type_byte_size);
  //-----------------------------------------------------------------------
  uint8_t load_uint8(uint8_t *buf);
  uint16_t load_uint16(uint8_t *buf);
  uint32_t load_uint32(uint8_t *buf);
  uint64_t load_uint64(uint8_t *buf);
  uint64_t load_value(uint8_t * buf,  uint8_t type_byte_size);
  //-----------------------------------------------------------------------
  void trim(char * string);
  int strpos(char *hay, char *needle, int offset);
  //-----------------------------------------------------------------------
  uint64_t file_open(const char * file_name, FILE ** file, char * mode);
  bool file_close(const char * file_name, FILE * file);
  //-----------------------------------------------------------------------
  void system_call(const char * command, char * result);
  unsigned long get_line_count(char * file_name);
  //-----------------------------------------------------------------------

#endif /* UTIL_MY_UTIL_H_ */
//---------------------------------------------------------------------------
