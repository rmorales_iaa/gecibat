/*
 ============================================================================
 Name        : gecibat.c
 Author      : Rafael Morales
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */
//============================================================================
#include "type_definition.h"
#include "version.h"
#include "global_definition.h"
#include "logger/logger.h"
#include "util/my_util.h"
#include "fits/fits.h"
//============================================================================
#include <stdbool.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
//============================================================================
FILE * csv_file = NULL;
char version[128];
extern uint64_t processed_record_count;

//elapsed time measuring
clock_t start_time_stamp;
double elapsedTime;

//simple conversion activated flag
bool simple_conversion = false;

//----------------------------------------------------------------------------
void get_version(void) {
  strcat(version, "gecibat version: ");
  strcat(version, GECIBAT_C_VERSION_MAYOR);
  strcat(version, ".");
  strcat(version, GECIBAT_C_VERSION_MINOR);
  strcat(version, ".");
  strcat(version, GECIBAT_C_VERSION_COMPILATION);
  strcat(version, " (");
  strcat(version, GECIBAT_C_VERSION_DATE);
  strcat(version, ")");
}
//----------------------------------------------------------------------------
void set_time_zone(void){
  //set time zone. See https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
  setenv("TZ", "Etc/UTC", 1);
  tzset();
}
//----------------------------------------------------------------------------
void initial_tasks(void){
  //time management
  set_time_zone();
  start_time_stamp = clock();

  //version
  *version = 0;
  get_version();
}
//----------------------------------------------------------------------------
void final_tasks(void){

  //elapsed time
  char s[64];
  clock_t start_time_end = clock();
  double elapsedTime = (double)(start_time_end - start_time_stamp) / CLOCKS_PER_SEC;
  snprintf(s, sizeof(s), "%f", elapsedTime);
  add_string_seq_to_logger(3, "gecibat elapsed time:: ", s," (s)");

  fflush(stdout);
  fflush(stderr);
}
//----------------------------------------------------------------------------
void update_number_of_records(unsigned long int csv_line_count, char * record) {

  char buf[1024];
  buf[0]='\0';
  snprintf(buf, 1024+1, "%lu", csv_line_count);

  record[0]='\0';

  strcpy(record, "NAXIS2  = ");

  int pad_count = 20-strlen(buf);
  for(int i=0;i<pad_count;++i)
	  strcat(record, " ");
  strcat(record,buf);
  strcat(record, " / number of records                              ");
}
//----------------------------------------------------------------------------
bool real_work(char * csv_file_name
		     , char * fits_file_name
			 , int csv_header_count
			 , unsigned long int csv_line_count) {
  char line[CSV_LINE_SIZE];
  char s[64];
  bool continue_processing = true;
  uint64_t line_count = 0;
  uint64_t csv_file_byte_size = file_open(csv_file_name, &csv_file, "rt");
  char record_NAXIS2[1024];

  //open the fits file
  if (!fits_create(fits_file_name, "w")) return false;

  //write min header
  if (!fits_write_min_header()) return false;

  //update the number of records
  update_number_of_records(csv_line_count, record_NAXIS2);

  if (simple_conversion) {
	if (!fits_write_table_header_simple_conversion(record_NAXIS2)) return false;
  }
  else {
	if (!fits_write_table_header(record_NAXIS2)) return false;
  }

  //parse the csv file
  if (csv_file_byte_size != 0) {
	while(continue_processing){
 	  ++line_count;
	  if (fgets(line, CSV_LINE_SIZE, csv_file) != NULL){
	    if (csv_header_count < line_count){
		  if (simple_conversion){
		    if(!process_record_simple_convesion(line,line_count)) continue_processing = false;
		  }
		  else
		    if(!process_record(line,line_count)) continue_processing = false;
		 }
      }
	  else continue_processing = false;
	}
	uint64_t curent_pos =  ftell(csv_file);
	if ((curent_pos > 0) && (csv_file_byte_size != curent_pos)) {
	  snprintf(s, sizeof(s), "%ld", curent_pos);
	  add_string_seq_to_logger(5, "File: ", csv_file_name,"' partially processed. Remain: ", s, "(bytes)");
	}
	else {
	  snprintf(s, sizeof(s), "%ld", line_count);
	  add_string_seq_to_logger(3, "Processed: ", s," lines");
	  snprintf(s, sizeof(s), "%ld", processed_record_count);
	  add_string_seq_to_logger(3, "Written  : ", s," FITS records");
	}

	uint64_t remain_byte_size_in_block = BLOCK_BYTE_SIZE - (processed_record_count * FITS_TABLE_RECORD_SIZE % BLOCK_BYTE_SIZE);
	if (remain_byte_size_in_block == BLOCK_BYTE_SIZE) remain_byte_size_in_block = 0;
	finish_binary_table(remain_byte_size_in_block);
	fits_close(fits_file_name);
	file_close(csv_file_name,csv_file);
	return true;
  }
  else {
	fits_close(fits_file_name);
    return false;
  }
}
//----------------------------------------------------------------------------
int main(int argc, char *argv[]) {
  initial_tasks();
  add_string_to_logger("gecibat: Gaia csv to FITS binary table in stream mode. It can manage huge tables(GiB)");
  add_string_to_logger(version);

  if ((argc != 3) && (argc != 4)) {

    add_string_to_logger("Error in user's argument. Expecting 2 or 3 arguments");
	add_string_to_logger("	Syntax   : gecibat gaia_csv_filename fits_filename [simple_conversion] [divider]");
	add_string_to_logger("	Example 1: Convert the cv file: 'gaia.csv' to FITS binary table 'gaia.fits'");
	add_string_to_logger("	The FITS format can be reviewed (and modified) in file 'fits.c' struct: FITS_RECORD_TABLE_HEADER");
	add_string_to_logger("	           It is asummed the CSV has 4 columns, a line with a header and blank as divider:");
	add_string_to_logger("	             source_id ra dec phot_g_mean_flux");
	add_string_to_logger("	gecibat gaia.csv gaia.fits\n");

	add_string_to_logger("  Example 2: same as Example 1 but assuming format:");
	add_string_to_logger("		   FIELD_X FIELD_Y INDEX_RA INDEX_DEC");
	add_string_to_logger("	gecibat gaia.csv gaia.fits 1\n");

	final_tasks();
	add_string_to_logger("gecibat ends with some error");
	return EXIT_FAILURE;
  }

  char * csv_file_name = argv[1];
  char * fits_file_name = argv[2];

  add_string_seq_to_logger(3, "Using csv file :'", csv_file_name, "'");
  add_string_seq_to_logger(3, "Using fits file:'", fits_file_name,"'");

  if (argc == 4){
	  simple_conversion = true;
	  add_string_seq_to_logger(1,"Using simple conversion. Assuming a line for header and column order: FIELD_X FIELD_Y INDEX_RA INDEX_DEC");
  }

  //get the number of rows
  unsigned long int csv_line_count = get_line_count(csv_file_name);
  if (csv_line_count == 0) {
    add_string_to_logger("gecibat ends with some error");
	final_tasks();
	return EXIT_FAILURE;
  }
  else {
    if (!real_work(csv_file_name, fits_file_name, 1, csv_line_count-1)){
	  add_string_to_logger("gecibat ends with some error");
	  final_tasks();
	  return EXIT_FAILURE;
	}
    add_string_to_logger("gecibat ends sucessfully");
  }

  final_tasks();
  return EXIT_SUCCESS;
}
//============================================================================
