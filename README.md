gecibat: Gaia Edr3 Csv to fIts Binary table in stream mode
Conversion from CSV to FITS binary table in C code
gecibat does not use external libraries (i.e. CFITSIO), just follow the FITS standard.
It runs in stream mode (one record each), so it can manage large catalogs
Adapted for GAIA EDR 3, but it can be easily extended to other large catalogs (see below)
The CSV file has been obtained from a local database of GAIA EDR 3. This is a NoSQL database (mongoDB) feeded by the raw GAIA data:
http://cdn.gea.esac.esa.int/Gaia/gedr3/gaia_source/

The exported fields of the database and present in he CSV file are:

    _id
    ra
    dec
    phot_g_mean_flux


Guide to adapt gecibat to another catalog:

  Check columns format and byte size in:
  See "FITS standard Version 4.0 2016 July 22" Sectin 7.3 Binary table extension
  
  Modify: string array 'FITS_RECORD_TABLE_HEADER' (fits.c)

      Add the required columns and its format ('TTYPEX' and 'TFORMX')
  
      Update properly the number of total entries of string array in
      FITS_RECORD_TABLE_HEADER_COUNT 

      Update the number of columns in
      TFIELDS

      Sum of bytes of all columns and set 
      FITS_TABLE_RECORD_SIZE (fits.h)
       
      Also put the sum as value of record 'NAXIS1' 
      
      Put the number of records in 'NAXIS2'


Example for GAIA EDR 3

```c
#define FITS_RECORD_TABLE_HEADER_COUNT 17
char * FITS_RECORD_TABLE_HEADER[FITS_RECORD_TABLE_HEADER_COUNT] = {
    "XTENSION= 'BINTABLE'           / binary table extension                         "    //0
  , "BITPIX  =                    8 / array data type                                "    //1
  , "NAXIS   =                    2 / number of array dimensions                     "    //2
  , "NAXIS1  =                   32 / byte size of each record (all columns)         "    //3
  , "NAXIS2  =           1811709771 / number of records                              "    //4
  , "PCOUNT  =                    0 / number of group parameters                     "    //5
  , "GCOUNT  =                    1 / number of groups                               "    //6
  , "TFIELDS =                    4 / number of columns                              "    //7
  , "TTYPE1  = 'ID      '                                                            "    //8
  , "TFORM1  = 'K       '                                                            "    //9
  , "TTYPE2  = 'RA      '                                                            "    //10
  , "TFORM2  = 'D       '                                                            "    //11
  , "TTYPE3  = 'DEC     '                                                            "    //12
  , "TFORM3  = 'D       '                                                            "    //13
  , "TTYPE4  = 'FLUX    '                                                            "    //14
  , "TFORM4  = 'D       '                                                            "    //15
  , "END                                                                             "    //16
};

//same value as record 'NAXIS1' in array 'FITS_RECORD_TABLE_HEADER'
#define FITS_TABLE_RECORD_SIZE  32
```
